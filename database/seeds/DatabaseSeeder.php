<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // seeder del sistema viejo

            $this->call('UserTypeSeeder');// usuarios
            $this->call('AdminSeeder');// administrador
            $this->call('Grupo_FisiologicoSeeder');// grupo
        
    //tablas madres
        //$this->call(PropietariosSeeder::class);
        //$this->call(CriadoresSeeder::class);
        //$this->call(Grupo_FisiologicoSeeder::class);        
    //tablas con primera relacion
        //$this->call(FincasSeeder::class);
        //$this->call(AnimalesSeeder::class);
        //$this->call(Arbol_GenealogicoSeeder::class);
        //$this->call(VeterinariosSeeder::class);
    //tablas relacionales
        //$this->call(Produccion_LacteaSeeder::class);
        //$this->call(Palpacion_RectalSeeder::class);
        //$this->call(VacunacionSeeder::class);
        //$this->call(Revision_GinecologicaSeeder::class);
    }
}
