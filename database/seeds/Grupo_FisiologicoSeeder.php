<?php

use Illuminate\Database\Seeder;
use App\Models\Grupo_Fisiologico;

class Grupo_FisiologicoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


    $array = array(
        0 => array(
            'nombre' => 'Ternero',
            'sexo' => 'Macho',
            'edad' => '0'
        ),
        1 => array(
            'nombre' => 'Novillo',
            'sexo' => 'Macho',
            'edad' => '0'
        ),
        2 => array(
            'nombre' => 'Ternera',
            'sexo' => 'Hembra',
            'edad' => '0'
        ),
        3 => array(
            'nombre' => 'Becerro',
            'sexo' => 'Macho',
            'edad' => '0'
        ),
        4 => array(
            'nombre' => 'Maute',
            'sexo' => 'Macho',
            'edad' => '0'
        ),
        5 => array(
            'nombre' => 'Mauta',
            'sexo' => 'Hembra',
            'edad' => '0'
        ),
        6 => array(
            'nombre' => 'Vaca',
            'sexo' => 'Hembra',
            'edad' => '0'
        ),
        7 => array(
            'nombre' => 'Toro',
            'sexo' => 'Macho',
            'edad' => '0'
        ),
        8 => array(
            'nombre' => 'Toretes',
            'sexo' => 'Macho',
            'edad' => '0'
        ),
    );


        $count = count($array);

        for ($i = 0; $i<$count; $i++) {

            App\Models\Grupo_Fisiologico::create([
                'nombre' => $array[$i]['nombre'],
                'sexo' => $array[$i]['sexo'],
                'edad' => $array[$i]['edad'],
            ]);

        }
    }
}
