<?php

use Illuminate\Database\Seeder;
use App\Models\UserType;

class UserTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $typeUsers = array(
            "Administrador",
            "Coordinador",
            "Supervisor",
            "Veterinario",
            "Auxiliar",
            "Propietario"
        );

        $countUsers = count($typeUsers);

        for ($i = 0; $i<$countUsers; $i++) {

            UserType::create([
            'descripcion' => $typeUsers[$i]
            ]);
        }
    }
}