<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\UserType;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userType = UserType::where("descripcion","Administrador")->first()->id;

        $user = User::create([
            'name' => "Hernan Abreu",
            'usuario' => "djherchino",
            'perfil' => "Admin",
            'ci' => "16576717",
            'email' => "herchino53@gmail.com",
            'password' => bcrypt("123456789"),
            'user_type_id' => $userType,
        ]);

        $user->save();

    }
}