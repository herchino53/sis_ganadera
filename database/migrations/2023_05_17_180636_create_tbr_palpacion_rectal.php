<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbrPalpacionRectal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbr_palpacion_rectal', function (Blueprint $table) {
            $table->increments('id');
            $table->text('observacion');
            $table->date('fecha');

            $table->integer('id_animal')->nullable()->unsigned();
            $table->foreign('id_animal')->references('id')->on('tbl_animales')->onDelete('cascade');

            $table->integer('id_veterinario')->nullable()->unsigned();
            $table->foreign('id_veterinario')->references('id')->on('tbl_veterinarios')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbr_palpacion_rectal');
    }
}
