<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblAnimales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_animales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo_reg');
            $table->string('nombre');
            $table->string('sexo');
            $table->string('fecha_nacimiento');
            $table->string('color');
            $table->integer('peso_nacer');
            $table->integer('peso_destete');
            $table->integer('peso_18_meses');
            $table->integer('peso_primer_parto');

            $table->integer('id_finca')->nullable()->unsigned();
            $table->foreign('id_finca')->references('id')->on('tbl_fincas')->onDelete('cascade');

            $table->integer('id_criadores')->nullable()->unsigned();
            $table->foreign('id_criadores')->references('id')->on('tbl_criadores')->onDelete('cascade');

            $table->integer('id_grupo_fisiologico')->nullable()->unsigned();
            $table->foreign('id_grupo_fisiologico')->references('id')->on('tbl_grupo_fisiologico')->onDelete('cascade'); 

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_animales');
    }
}
