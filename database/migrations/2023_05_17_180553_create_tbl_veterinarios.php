<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblVeterinarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_veterinarios', function (Blueprint $table) {
            $table->increments('id');
            $table->text('codigo_reg');
            $table->text('nombre');
            $table->text('apellido');
            $table->integer('cedula');
            $table->integer('telefono');
            $table->text('correo');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_veterinarios');
    }
}
