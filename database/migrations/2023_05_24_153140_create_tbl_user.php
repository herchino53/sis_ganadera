<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_user', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            //$table->string('apellido');
            $table->string('username');
            $table->string('perfil')->nullable();
            //$table->string('foto')->nullable();
            //$table->string('estado')->nullable();
            $table->integer('ci');//->unique();
            $table->string('email');//->unique();
            $table->string('email_verified_at')->nullable();
            $table->string('password');

            $table->integer('user_type_id')->nullable()->unsigned();
            $table->foreign('user_type_id')->references('id')->on('tbl_user_types')->onDelete('cascade');

            $table->rememberToken();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_user');
    }
}
