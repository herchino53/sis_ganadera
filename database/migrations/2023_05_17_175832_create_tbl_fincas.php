<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblFincas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_fincas', function (Blueprint $table) {
            $table->increments('id');
            $table->text('codigo_reg');
            $table->text('nombre');
            $table->text('direccion');
            $table->integer('telefono');

            $table->integer('id_propietario')->nullable()->unsigned();
            $table->foreign('id_propietario')->references('id')->on('tbl_propietarios')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_fincas');
    }
}
