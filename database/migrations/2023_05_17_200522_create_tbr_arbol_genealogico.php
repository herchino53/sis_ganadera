<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbrArbolGenealogico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbr_arbol_genealogico', function (Blueprint $table) {
            $table->increments('id');
            $table->text('codigo_reg');

            $table->integer('id_animal')->nullable()->unsigned();
            $table->foreign('id_animal')->references('id')->on('tbl_animales')->onDelete('cascade');

            $table->integer('id_padre')->nullable()->unsigned();
            $table->foreign('id_padre')->references('id')->on('tbl_animales')->onDelete('cascade');

            $table->integer('id_madre')->nullable()->unsigned();
            $table->foreign('id_madre')->references('id')->on('tbl_animales')->onDelete('cascade');

            $table->integer('id_abuelo_paterno')->nullable()->unsigned();
            $table->foreign('id_abuelo_paterno')->references('id')->on('tbl_animales')->onDelete('cascade');

            $table->integer('id_abuela_paterna')->nullable()->unsigned();
            $table->foreign('id_abuela_paterna')->references('id')->on('tbl_animales')->onDelete('cascade');

            $table->integer('id_abuelo_materna')->nullable()->unsigned();
            $table->foreign('id_abuelo_materna')->references('id')->on('tbl_animales')->onDelete('cascade');

            $table->integer('id_abuela_materna')->nullable()->unsigned();
            $table->foreign('id_abuela_materna')->references('id')->on('tbl_animales')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbr_arbol_genealogico');
    }
}
