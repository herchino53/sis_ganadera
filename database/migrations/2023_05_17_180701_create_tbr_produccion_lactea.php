<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbrProduccionLactea extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbr_produccion_lactea', function (Blueprint $table) {
            $table->increments('id');
            $table->text('codigo_reg');
            $table->integer('litros');
            $table->date('fecha_registro');

            $table->integer('id_animal')->nullable()->unsigned();
            $table->foreign('id_animal')->references('id')->on('tbl_animales')->onDelete('cascade');             
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbr_produccion_lactea');
    }
}
