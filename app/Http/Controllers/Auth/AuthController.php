<?php

namespace App\Http\Controllers\Auth;

//use App\User;
use App\Model\User;

use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    protected $username = 'ci'; // para logear con cédula

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */


    public function loginUsername()
    {
        //return "AuthController loginUsername";
        return property_exists($this, 'username') ? $this->username : 'ci';
    }


    protected function validator(array $data)
    {
        //dd('validator');

        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            //'apellido' => ['required', 'string', 'max:255'],
            'ci' => ['required', 'integer', 'min:599999', 'max:59999999'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:tbl_user'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],

            /*
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            */
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        dd('AuthController - create');

        return User::create([
            'name' => $data['name'],
            //'apellido' => $data['apellido'],
            //'perfil' => $data['perfil'],
            'ci' => $data['ci'],
            'email' => $data['email'],
            //'email_verified_at' => $data['email_verified_at'],
            'password' => bcrypt($data['password']),

            /*
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            */
        ]);
    }

    /*Redireccionar dependiendo del tipo de usuario que se esta logeando*/

   protected function login(Request $request) {

        return "AuthController login";
    }

}
