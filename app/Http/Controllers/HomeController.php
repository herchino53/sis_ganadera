<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\User;
use App\Models\UserType;
use DB;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $usuario = Auth::user();
        dd($usuario);

        switch ($usuario->user_type_id) {
            case 1:// administrador
            case 2: //Director
            case 3:// EAP
            case 4: //Docente
            case 7: //Gobernador
                return redirect('/dashboard');
                break;
            // otros usuarios
            case 5: //Representante
            case 6: //Secretaria
                return redirect('/interna');
                break;
            
            

            //otros salir
            default:
                return redirect('/logout');
                break;
        }

        return view('home');
    }
}
