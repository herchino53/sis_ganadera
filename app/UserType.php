<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    protected $table = 'tbl_user_types';

    protected $primaryKey = 'id';

    protected $fillable = array('id','descripcion');


    public function subject() {
        return $this->hasMany('app\User', 'user_id');
    } 
}
