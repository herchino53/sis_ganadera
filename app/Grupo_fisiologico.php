<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grupo_fisiologico extends Model
{
    protected $table = 'tbl_grupo_fisiologico';

    protected $primaryKey = 'id';

    protected $fillable = array('id','nombre','sexo','edad');
}
