<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Palpacion_Rectal extends Model
{
    protected $table = 'tbr_palpacion_rectal';

    protected $primaryKey = 'id';

    protected $fillable = array('id','id_animal','id_veterinario','observacion','fecha');
}
