<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Grupo_Fisiologico extends Model
{
    protected $table = 'tbl_grupo_fisiologico';

    protected $primaryKey = 'id';

    protected $fillable = array('id','nombre','sexo','edad');
}
