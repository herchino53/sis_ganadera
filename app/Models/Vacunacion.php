<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vacunacion extends Model
{
    protected $table = 'tbr_vacunacion';

    protected $primaryKey = 'id';

    protected $fillable = array('id','id_animal','id_veterinario','nombre','principio_activo','dosis','fecha');
}
