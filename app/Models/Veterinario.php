<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Veterinario extends Model
{
    protected $table = 'tbl_veterinarios';

    protected $primaryKey = 'id';

    protected $fillable = array('id','codigo_reg','nombre','apellido','cedula','telefono','correo');
}
