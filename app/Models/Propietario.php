<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Propietario extends Model
{
    protected $table = 'tbl_propietarios';

    protected $primaryKey = 'id';

    protected $fillable = array('id','nombre','apellido','cedula','telefono','correo');
}
