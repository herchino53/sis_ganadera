<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Criador extends Model
{
    protected $table = 'tbl_criadores';

    protected $primaryKey = 'id';

    protected $fillable = array('id','nombre','apellido','cedula','telefono','correo');
}
