<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;


//class User extends Model
class User extends Authenticatable
{
    use Notifiable;


    protected $table = 'tbl_user';

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name',
                            //'apellido',
                            'username',
                            'perfil',//'foto','estado',
                            'ci','email',
                            'email_verified_at',
                            'password'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

        public function type_user() {
        return $this->belongsTo('sis_ganadera\Models\UserType', 'user_type_id');
    }
}
