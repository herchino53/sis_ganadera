<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Finca extends Model
{
    protected $table = 'tbl_fincas';

    protected $primaryKey = 'id';

    protected $fillable = array('id','codigo_reg','id_propietario','nombre','direccion','telefono');
}
