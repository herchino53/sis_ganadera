<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Producion_Lactea extends Model
{
    protected $table = 'tbr_produccion_lactea';

    protected $primaryKey = 'id';

    protected $fillable = array('id','codigo_reg','id_animal','litros','fecha_registro');
}
