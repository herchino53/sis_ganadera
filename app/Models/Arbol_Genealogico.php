<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Arbol_Genealogico extends Model
{
    protected $table = 'tbr_arbol_genealogico';

    protected $primaryKey = 'id';

    protected $fillable = array('id','id_animal','codigo_reg','id_padre','id_madre','id_abuelo_paterno','id_abuela_paterna','id_abuelo_materna','id_abuela_materna');
}
