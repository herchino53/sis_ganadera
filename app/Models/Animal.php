<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Animal extends Model
{
    protected $table = 'tbl_animales';

    protected $primaryKey = 'id';

    protected $fillable = array('id','codigo_reg','id_finca','id_criadores','id_grupo_fisiologico','nombre','sexo','fecha_nacimiento','color','peso_nacer','peso_destete','peso_18_meses','peso_primer_parto'
}
