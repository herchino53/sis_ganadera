<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Revision_Ginecologica extends Model
{
    protected $table = 'tbr_revision_ginecologica';

    protected $primaryKey = 'id';

    protected $fillable = array('id','id_animal','id_veterinario','diagnostico','observacion','receta_medica','fecha');
}