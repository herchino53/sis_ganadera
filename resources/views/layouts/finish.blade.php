<!DOCTYPE html>
<html lang="en">
<head>
  @include('layouts.style')
</head>
<body>

  <div class="row hidden-xs color-login">

      <div class="col-xs-12 col-md-10 col-md-offset-1">
         <div class="row">
            <div class="col-xs-2 logo-ucv-home">
               <img src="{{asset('img/logos/logoMirandaBlanco.png')}}" alt="..." class="img-circle profile">
            </div>
            
            <div class="col-xs-8 text-center top-20 mg-0">
                <img id="logo-login" class="img-responsive" src="{{asset('img/logos/favico.ico')}}">
                <h3 class = "top-20 color-title-login">
                  TITULO
                </h3>
            </div>
            <div class="col-xs-2 fau-logo">
               <img src="{{asset('img/logos/logoMirandaBlanco.png')}}" class="img-responsive" />
            </div>

         </div>
      </div>

   </div>


   <div class="row hidden-sm hidden-md hidden-lg color-login">
      <div class="col-xs-12">
         <div class="row">
            <div class="col-xs-12 text-center top-20 mg-0">
                <img id="logo-login" class="img-responsive" src="favico.ico" alt="Opine Sobre Docencia Logo">
                <h3 class = "top-20 color-title-login">
                  © EDDEM 2020 - Evaluación del Desempeño del Docente del Estado Miranda desde el acompañamiento Pedagógico.
                </h3>
            </div>
         </div>
      </div>
   </div>

    @yield('content')
  
      <div id="footer">
          <p>
            © EDDEM 2020 - Evaluación del Desempeño del Docente del Estado Miranda desde el acompañamiento Pedagógico.
          </p>
      </div>

  <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>

    <script type="text/javascript" src="{!! asset('js/dinamic-footer.js') !!}"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

   
</body>
</html>
