<!DOCTYPE html>
<html lang="es">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('layouts.style')
</head>

<body>
    <header class="container-fluid menu-home">
                   
    </header>
    
 
    <div class="block-menu"></div>
    <div class="portal">
        <p>Portal del Usuario</p>
    </div>



    @yield('content')
    <footer>
    
   </footer>
    <!-- JavaScripts -->


    <script src="js/jquery-min.js" type="text/javascript"></script>  

    <script src="js/bootstrap.min.js"></script> 
    <script src="js/Chart.min.js" type="text/javascript" "></script>

    <script type="text/javascript" src="{!! asset('js/jquerymask.js') !!}"></script>

    <script type="text/javascript" src="{!! asset('js/menu.js') !!}"></script>
     @yield('scripts')


 <script type="text/javascript">
function reply_click(clicked_id)
{
    alert(clicked_id);
}
</script>


</body>
</html>