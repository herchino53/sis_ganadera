            <!-- sidebar menu -->
               
            @unless(Auth::check() )
              <div id="sidebar-menu2" class="main_menu_side hidden-print main_menu top-30">
                 <div class="menu_section">
                    <h3>Opciones:</h3>
                    <ul class="nav side-menu">
                      {{--  Vista para estudiantes que no tienen que logearse --}}
                        <li>
                           @yield('link')
                        </li>
                 </div>
              </div>
            @endunless

@if (Auth::check())
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                  <div class="menu_section">
                  <h3 class="top-20">General</h3>
                  <ul class="nav side-menu">
                     <li>
                        <a href="{{ url('/interna') }}"><i class="fa fa-home"></i> Inicio </a>
                     </li>
                  </ul>
                  <hr class = "menu-hr">
                  </div>
                  <div class="menu_section">
                  <h3>Visualizar Evaluaciones</h3>
                  <ul class="nav side-menu">
                    {{--  Vista para estudiantes que no tienen que logearse --}}
                     @unless(Auth::check() )
                        <li>
                           <a href= "{{ action('InternalController@pickKnowledgeAreaEvaluation')}}"><i class="fa fa-file-text-o"></i> Elegir profesores a evaluar. </a>
                        </li>
                     @endunless

<!-- @ if( (Auth::user()->type_user->description == 'Directivo')) -->
@if( 
    //$usuario->user_type_id == 1 || //administrador
     $usuario->user_type_id == 2 //|| //Director
     //$usuario->user_type_id == 3 || //EAP
     //$usuario->user_type_id == 4 || // Docente
     //$usuario->user_type_id == 5 || // Representante
     //$usuario->user_type_id == 6 || // Secretaria
     //$usuario->user_type_id == 7  // Gobernador
     ) 

                        <!--                         <li>
                           <a href= "{{ action('InternalController@pickKnowledgeAreaEvaluation')}}"><i class="fa fa-users"></i> Evaluación global por Áreas de Conocimiento </a>
                        </li>
                        <li>
                           <a href= "{{ action('InternalController@pickSubKnowledgeAreaEvaluation')}}"><i class="fa fa-users"></i> Evaluación global por Sub Áreas de Conocimiento </a>
                        </li> -->
                        <li>
                           <a href= "{{ action('InternalController@pickUserEvaluation')}}"><i class="fa fa-user"></i> Evaluación individual de docentes </a>
                        </li>

@endif
                     
<!-- @ if( Auth::user()->type_user->description == 'Coordinador_areas') -->
@if( 
     //$usuario->user_type_id == 1 || //administrador
     //$usuario->user_type_id == 2 || //Director
     $usuario->user_type_id == 3 || //EAP
     //$usuario->user_type_id == 4 || // Docente
     //$usuario->user_type_id == 5 || // Representante
     $usuario->user_type_id == 6 //|| // Secretaria
     //$usuario->user_type_id == 7  // Gobernador
     ) 
                        <li>
                           <a href= "{{ action('InternalController@pickKnowledgeAreaEvaluation')}}"><i class="fa fa-users"></i> Evaluación global por Áreas de Conocimiento </a>
                        </li>
                        <li>
                           <a href= "{{ action('InternalController@pickSubKnowledgeAreaEvaluation')}}"><i class="fa fa-users"></i> Evaluación global por Sub Áreas de Conocimiento </a>
                        </li>
                        <li>
                           <a href= "{{ action('InternalController@pickUserArea')}}"><i class="fa fa-user"></i> Evaluación individual por Áreas de Conocimiento </a>
                        </li>
                         <li>
                           <a href= "{{ action('InternalController@pickUserSubArea')}}"><i class="fa fa-user"></i> Evaluación individual por Sub Áreas de Conocimiento </a>
                        </li>


@endif

<!-- @ if( Auth::user()->type_user->description == 'Coordinador_areas') -->
@if( 
     //$usuario->user_type_id == 1 || //administrador
     //$usuario->user_type_id == 2 || //Director
     $usuario->user_type_id == 3 || //EAP
     //$usuario->user_type_id == 4 || // Docente
     //$usuario->user_type_id == 5 || // Representante
     $usuario->user_type_id == 6 //|| // Secretaria
     //$usuario->user_type_id == 7  // Gobernador
     ) 
                  <div class="menu_section">
                  <hr class = "menu-hr">

                  <h3>VIZUALIZAR EVALUACIÓN COMPARATIVA</h3>
                  <ul class="nav side-menu">
                    <li>
                       <a href= "{{ action('InternalController@pickCompareAreaEvaluation')}}"><i class="fa fa-user"></i> Evaluación comparativa de Áreas de Conocimiento </a>
                    </li>

                    <li>
                       <a href= "{{ action('InternalController@pickCompareSubAreaEvaluation')}}"><i class="fa fa-user"></i> Evaluación comparativa de Sub Áreas de Conocimiento </a>
                    </li>
                     <li>
                       <a href= "{{ action('InternalController@pickCompareUserEvaluation')}}"><i class="fa fa-user"></i> Evaluación comparativa individual de profesores </a>
                    </li>
                   
                  </ul>
                  </div>

                  <div class="menu_section">
                      <hr class = "menu-hr">
                      <h3>GENERACIÓN DE REPORTES</h3>
                     <ul class="nav side-menu">
                        <li>
                           <a href= "{{ action('ReportController@reportAreaForm')}}"><i class="fa fa-users"></i> Reportes de Áreas de Conocimiento  </a>
                        </li>
                        <li>
                           <a href= "{{ action('ReportController@reportSubAreaForm')}}"><i class="fa fa-users"></i> Reportes de Sub Áreas de Conocimiento </a>
                        </li>
                        <li>
                           <a href= "{{ action('ReportController@reportTeacherForm')}}"><i class="fa fa-user"></i> Reportes de Profesores </a>
                        </li>
                         <hr class = "menu-hr">
                     </ul>
                  </div>
@endif

<!-- @ if( Auth::user()->type_user->description == 'Coordinador_sub_areas') -->
@if( 
     //$usuario->user_type_id == 1 || //administrador
     //$usuario->user_type_id == 2 || //Director
     $usuario->user_type_id == 3 || //EAP
     //$usuario->user_type_id == 4 || // Docente
     //$usuario->user_type_id == 5 || // Representante
     $usuario->user_type_id == 6 //|| // Secretaria
     //$usuario->user_type_id == 7  // Gobernador
     ) 
                <li>
                   <a href= "{{ action('InternalController@pickSubKnowledgeAreaEvaluation')}}"><i class="fa fa-users"></i> Evaluación global por Sub Áreas de Conocimiento </a>
                </li>

                <li>
                   <a href= "{{ action('InternalController@pickUserSubArea')}}"><i class="fa fa-user"></i> Evaluación individual por Sub Áreas de Conocimiento </a>
                </li>
@endif

<!-- @ if( Auth::user()->type_user->description == 'Coordinador_sub_areas') -->
@if( 
     //$usuario->user_type_id == 1 || //administrador
     //$usuario->user_type_id == 2 || //Director
     $usuario->user_type_id == 3 || //EAP
     //$usuario->user_type_id == 4 || // Docente
     //$usuario->user_type_id == 5 || // Representante
     $usuario->user_type_id == 6 //|| // Secretaria
     //$usuario->user_type_id == 7  // Gobernador
     ) 
              <div class="menu_section">
                  <hr class = "menu-hr">
                 <h3>VIZUALIZAR EVALUACIÓN COMPARATIVA</h3>
                 <ul class="nav side-menu">
                    <li>
                       <a href= "{{ action('InternalController@pickCompareSubAreaEvaluation')}}"><i class="fa fa-user"></i> Evaluación comparativa de Sub Áreas de Conocimiento </a>
                    </li>
                     <li>
                       <a href= "{{ action('InternalController@pickCompareUserEvaluation')}}"><i class="fa fa-user"></i> Evaluación comparativa individual de profesores </a>
                    </li>
                 </ul>
              </div>

              <div class="menu_section">
                  <hr class = "menu-hr">
                  <h3>GENERACIÓN DE REPORTES</h3>
                 <ul class="nav side-menu">
                    <li>
                       <a href= "{{ action('ReportController@reportSubAreaForm')}}"><i class="fa fa-users"></i> Reportes de Sub Áreas de Conocimiento </a>
                    </li>
                    <li>
                       <a href= "{{ action('ReportController@reportTeacherForm')}}"><i class="fa fa-user"></i> Reportes de Profesores </a>
                    </li>
                     <hr class = "menu-hr">
                 </ul>
              </div>
@endif

<!-- @ if( Auth::user()->type_user->description == 'Profesor') -->
@if( false
     //$usuario->user_type_id == 1 || //administrador
     //$usuario->user_type_id == 2 || //Director
     //$usuario->user_type_id == 3 || //EAP
     //$usuario->user_type_id == 4 //|| // Docente
     //$usuario->user_type_id == 5 || // Representante
     //$usuario->user_type_id == 6 || // Secretaria
     //$usuario->user_type_id == 7  // Gobernador
     ) 
                <li>
                   <a href= "{{ action('InternalController@pickTeacherEvaluation')}}"><i class="fa fa-user"></i> Revisar resultados de las evaluaciones </a>
                </li>
@endif
                  
              </ul>
           </div>

<!-- @ if( Auth::user()->type_user->description == 'Profesor') -->
@if( false
     //$usuario->user_type_id == 1 || //administrador
     //$usuario->user_type_id == 2 || //Director
     //$usuario->user_type_id == 3 || //EAP
     //$usuario->user_type_id == 4 || // Docente
     //$usuario->user_type_id == 5 || // Representante
     //$usuario->user_type_id == 6 || // Secretaria
     //$usuario->user_type_id == 7  // Gobernador
     ) 

                  <div class="menu_section">

                      <hr class = "menu-hr">
            
                     <h3>VIZUALIZAR EVALUACIÓN COMPARATIVA</h3>
                     <ul class="nav side-menu">
                        <li>
                           <a href= "{{ action('InternalController@pickCompareTeacherIndividual')}}"><i class="fa fa-user"></i> Evaluación comparativa individual </a>
                        </li>
                       
                     </ul>
                  </div>

                  <div class="menu_section">

                      <hr class = "menu-hr">
            
                     <h3>GENERACIÓN DE REPORTES</h3>
                     <ul class="nav side-menu">
                        <li>
                           <a href= "{{ action('ReportController@reportIndividualTeacherForm')}}"><i class="fa fa-user"></i> Generar reporte de evaluación </a>
                        </li>
                     </ul>
                  </div>
@endif
            
               
<!-- @ if( (Auth::user()->type_user->description == 'Directivo')) -->
@if( 
     //$usuario->user_type_id == 1 || //administrador
     $usuario->user_type_id == 2 //|| //Director
     //$usuario->user_type_id == 3 || //EAP
     //$usuario->user_type_id == 4 || // Docente
     //$usuario->user_type_id == 5 || // Representante
     //$usuario->user_type_id == 6 || // Secretaria
    // $usuario->user_type_id == 7  // Gobernador
     ) 
                  <div class="menu_section">

                      <hr class = "menu-hr">
            
                     <h3>VIZUALIZAR EVALUACIÓN COMPARATIVA</h3>
                     <ul class="nav side-menu">
                      <!--                         <li>
                         <a href= "{{ action('InternalController@pickCompareAreaEvaluation')}}"><i class="fa fa-users"></i> Evaluación comparativa de Áreas de Conocimiento  </a>
                      </li>
                      <li>
                         <a href= "{{ action('InternalController@pickCompareSubAreaEvaluation')}}"><i class="fa fa-users"></i> Evaluación comparativa de Sub Áreas de Conocimiento  </a>
                      </li> -->
                        <li>
                           <a href= "{{ action('InternalController@pickCompareUserEvaluation')}}"><i class="fa fa-user"></i> Evaluación comparativa individual de profesores </a>
                        </li>

                         <hr class = "menu-hr">
                       
                     </ul>
                  </div>
                  <div class="menu_section">
                     
                     <h3>GENERACIÓN DE REPORTES</h3>
                     <ul class="nav side-menu">
                      <!--                         <li>
                         <a href= "{{ action('ReportController@reportAreaForm')}}"><i class="fa fa-users"></i> Reportes de Áreas de Conocimiento  </a>
                      </li>
                      <li>
                         <a href= "{{ action('ReportController@reportSubAreaForm')}}"><i class="fa fa-users"></i> Reportes de Sub Áreas de Conocimiento </a>
                      </li> -->
                        <li>
                           <a href= "{{ action('ReportController@reportTeacherForm')}}"><i class="fa fa-user"></i> Reportes de Docentes </a>
                        </li>

                         <hr class = "menu-hr">
                       
                     </ul>
                  </div>
@endif
            </div>

@endif
           
            </div>
         </div>