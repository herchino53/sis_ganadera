            
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
               <div class="menu_section">
                  <h3>General</h3>
                  <ul class="nav side-menu">
                     <li>
                        <a href="{{ url('/dashboard') }}"><i class="fa fa-home"></i> Inicio </a>
                     </li>
                  </ul>
                  <hr class = "menu-hr">
               </div>


@if( 
    $usuario->user_type_id == 1 || //administrador
    $usuario->user_type_id == 2 || //Director
    //$usuario->user_type_id == 3 || //EAP
    $usuario->user_type_id == 4 //|| // Docente
    //$usuario->user_type_id == 5 || // Representante
    //$usuario->user_type_id == 6 || // Secretaria
    //$usuario->user_type_id == 7  // Gobernador
    )      
<!-- ficha docente -->
               <div class="menu_section">
                  <h3>Administrar Docentes</h3>
                  <ul class="nav side-menu">
                      <li>
                        <a href= "{{ action('FichaDocenteController@index')}}"><i class="fa fa-user"></i>Fichas Docentes </a>
                     </li>

                     <li>
                        <a href= "{{ action('FichaDocenteController@showCreateDocentesForm')}}">
                           <!-- <i class="fa fa-user-plus"> --><i class="fa fa-user"></i> Crear Ficha Docente
                        </a>
                     </li>                   
                      <hr class = "menu-hr">
                  </ul>
               </div>
@endif



@if( 
    $usuario->user_type_id == 1 || //administrador
    $usuario->user_type_id == 2 //|| //Director
    //$usuario->user_type_id == 3 || //EAP
    //$usuario->user_type_id == 4 || // Docente
    //$usuario->user_type_id == 5 || // Representante
    //$usuario->user_type_id == 6 || // Secretaria
    //$usuario->user_type_id == 7  // Gobernador
    ) 
<!-- ficha plantel -->
               <div class="menu_section">
                  <h3>Administrar Instituciones</h3>
                  <ul class="nav side-menu">
                      <li>
                        <a href= "{{ action('FichaInstitucionController@index')}}"><i class="fa fa-home"></i> Fichas Instituciones </a>
                     </li>

                     <li>
                        <a href= "{{ action('FichaInstitucionController@showCreateInstitucionForm')}}">
                           <!-- <i class="fa fa-user-plus"> --><i class="fa fa-home"></i> Crear Ficha Institucion
                        </a>
                     </li>
                   
                      <hr class = "menu-hr">
                    
                  </ul>
               </div>
@endif

@if( 
    $usuario->user_type_id == 1 || //administrador
    $usuario->user_type_id == 2 //|| //Director
    //$usuario->user_type_id == 3 || //EAP
    //$usuario->user_type_id == 4 || // Docente
    //$usuario->user_type_id == 5 || // Representante
    //$usuario->user_type_id == 6 || // Secretaria
    //$usuario->user_type_id == 7  // Gobernador
    ) 
<!-- instrumentos -->
               <div class="menu_section">
                  <h3>Administrar Instrumentos </h3>
                  <ul class="nav side-menu">
                      <li>
                        <a href= "{{ action('DashboardController@trabajando')}}"><i class="fa fa-book"></i> Instrumentos de Evaluación</a>
                     </li>

                     <li>
                        <a href= "{{ action('DashboardController@trabajando')}}">
                           <!-- <i class="fa fa-user-plus"> --><i class="fa fa-book"></i> Crear Instrumento de Evaluación
                        </a>
                     </li>
                   
                      <hr class = "menu-hr">
                    
                  </ul>
               </div>
@endif

@if( 
    $usuario->user_type_id == 1 || //administrador
    $usuario->user_type_id == 2 //|| //Director
    //$usuario->user_type_id == 3 || //EAP
    //$usuario->user_type_id == 4 || // Docente
    //$usuario->user_type_id == 5 || // Representante
    //$usuario->user_type_id == 6 || // Secretaria
    //$usuario->user_type_id == 7  // Gobernador
    ) 
<!-- usuarios -->
               <div class="menu_section">
                  <h3>Administrar usuarios</h3>
                  <ul class="nav side-menu">
                      <li>
                        <a href= "{{ action('DashboardController@showUsers')}}"><i class="fa fa-user"></i> Listar Usuarios </a>
                     </li>

                     <li>
                        <a href= "{{ action('DashboardController@showCreateUserForm')}}">
                           <!-- <i class="fa fa-user-plus"> --><i class="fa fa-user"></i> Crear Usuario
                        </a>
                     </li>
                      <hr class = "menu-hr">
                    
                  </ul>
               </div>
@endif


@if( 
    $usuario->user_type_id == 1 || //administrador
    $usuario->user_type_id == 2 || //Director
    //$usuario->user_type_id == 3 || //EAP
    //$usuario->user_type_id == 4 || // Docente
    //$usuario->user_type_id == 5 || // Representante
    $usuario->user_type_id == 6 || // Secretaria
    $usuario->user_type_id == 7  // Gobernador
    ) 
<!-- Reportes -->
               <div class="menu_section">
                  <h3>Administrar Reportes </h3>
                  <ul class="nav side-menu">
                      <li>
                        <a href= "{{ action('DashboardController@trabajando')}}"><i class="fa fa-book"></i> Reportes Docente</a>
                     </li>

                     <li>
                        <a href= "{{ action('DashboardController@trabajando')}}">
                           <!-- <i class="fa fa-user-plus"> --><i class="fa fa-book"></i> Reportes Institución
                        </a>
                     </li>
                   
                      <hr class = "menu-hr">
                    
                  </ul>
               </div>
@endif

            </div>
            <!-- sidebar menu -->