    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- <title>@yield('title')</title> -->
   <title>EDDEM - Evaluación del Desempeño Docente</title> 
<!-- header -->
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->

   
    <!-- Favicon -->
    <link rel="shortcut icon" href="{!! asset('neuboard/img/favicon.ico')!!}" type="image/x-icon">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet"
        href="{!! asset('neuboard/plugins/bootstrap/css/bootstrap.min.css') !!}?{!!substr(time(),-5)!!}">
    <!-- Fonts  -->
    <link rel="stylesheet" href="{!! asset('neuboard/css/font-awesome.min.css') !!}?{!!substr(time(),-5)!!}">
    <link rel="stylesheet" href="{!! asset('neuboard/css/simple-line-icons.css') !!}?{!!substr(time(),-5)!!}">
    <!-- CSS Animate -->
    <link rel="stylesheet" href="{!! asset('neuboard/css/animate.css') !!}?{!!substr(time(),-5)!!}">
    <!-- Daterange Picker -->
    <link rel="stylesheet"
        href="{!! asset('neuboard/plugins/daterangepicker/daterangepicker-bs3.css') !!}?{!!substr(time(),-5)!!}">
    <!-- Calendar demo -->
    <link rel="stylesheet" href="{!! asset('neuboard/css/clndr.css') !!}?{!!substr(time(),-5)!!}">
    <!-- Switchery -->
    <link rel="stylesheet" href="{!! asset('neuboard/plugins/switchery/switchery.min.css') !!}?{!!substr(time(),-5)!!}">




    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->

    <!-- <link href="https://fonts.googleapis.com/css?family=Raleway:400,600,700" rel="stylesheet"> -->

    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css"> -->

    <link href="{{ asset('build/css/app-c169a2659d.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
<!-- header -->