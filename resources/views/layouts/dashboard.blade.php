<!DOCTYPE html>
<html lang="en">
<head>
  @include('layouts.style')
</head>
<body class="nav-md">
   <div class="container body">
      <div class="main_container">
         <div class="col-md-3 left_col ">
            <div class="left_col scroll-view">
               
            <div class="clearfix"></div>
              <?php
                $usuario = Auth::user();
                $query = "SELECT * FROM user_types WHERE id =". $usuario->user_type_id;
                $rol = DB::select( DB::raw($query));
              ?>

            <!-- menu profile quick info -->
            <div class="profile clearfix top-20">
               <div class="profile_pic">
                  <!-- <img src="{{asset('img/logos/logoMirandaBlancoFX.png')}}" alt="..." class="img-circle profile"> -->
                  <img src="{{asset('img/logos/logoMirandaBlanco.png')}}" alt="..." class="img-circle profile">
               </div>
               <div class="profile_info">
                  <span>Bienvenido,</span>
                  <h2>{{$usuario->name}}</h2>
                   <h4>Rol: {{$rol[0]->description}}</h4>
               </div>
            </div>
            <!-- /menu profile quick info -->
            <br>
            <!-- MENU -->
              @include('layouts.menu')
            <!-- MENU -->
         </div>
      </div>

        <!-- top navigation -->
         <!-- top navigation -->
     
        <div class="top_nav">
          <div class="nav_menu">
            <nav>

              <div id="cintillo">
                <div class="row">
                  <div class="col-xs-10">
                    <img src="{{asset('img/logos/cintillo-gobierno-1368x68.png')}}" class="img-responsive" />
                  </div> 
                </div>
              </div>


            <div class="row">
                  <div class="col-xs-2">
                      <div class="nav toggle">
                          <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                      </div>
                  </div>
                  <div class="col-xs-3 col-sm-7 text-center">
                      <div class="row">
                        <div class="col-xs-11 text-center hidden-xs">
                          <div class="img-menu">
                            <img id="logo-login1" class="img-responsive" src="{{asset('img/logos/banner-ppal.png')}}">
                          </div>
                        </div>
                        <div class="col-xs-1">
                           <img  id="logo-login-miranda1" class="img-responsive" src="{{asset('img/logos/logoMirandaBlanco.png')}}"  />
                        </div>                         
                      </div>
                  </div>
                
                @if (Auth::check())
                  <div class="col-xs-7 col-sm-3">
                    <ul class="nav navbar-nav navbar-right">
                      <li class="">
                        <a href="" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                          <i class="fa fa-user"></i>  {{$usuario->name}}
                          <span class=" fa fa-angle-down"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-usermenu pull-right">
                           <li>
                           {{ Html::linkAction('DashboardController@editLoginUserForm', 'Editar Perfil', array($usuario->id)) }}    
                          </li>
                          <li><a href="{{ url('/logout') }}"><i class="fa fa-sign-out pull-right"></i>Salir</a></li>
                        </ul>
                       </li>
                     </ul>
                   </div>
                @endif
            </div>

            </nav>
          </div>
        </div>
      
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          @yield('content')
        </div>
      	
        <!-- /page content -->
      </div>
   </div>





  <!-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script> -->
  <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

  <script src="{!! asset('build/js/all-900fea4ba6.js') !!}"></script> 
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script> -->
  <script type="text/javascript" src="{!! asset('js/bootstrap-datepicker.min.js') !!}"></script>

  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.es.min.js"></script> -->
  <script type="text/javascript" src="{!! asset('js/bootstrap-datepicker.es.min.js') !!}"></script>

  <script type="text/javascript" src="{!! asset('js/displayChart.js') !!}"></script>
  <script type="text/javascript" src="{!! asset('js/dinamic-form.js') !!}"></script>
  <script type="text/javascript" src="{!! asset('js/selectDate.js') !!}"></script>
  <script type="text/javascript" src="{!! asset('js/dinamic-form-edit.js') !!}"></script>
  <script>   
    $('.date').datepicker({
          format: "dd/mm/yyyy",
          language: "es",
          startView: 3
    });
    $('.date').on('keydown',function(e){
          e.preventDefault();
    });
    $('.date-notificar').datepicker({
          format: "dd/mm/yyyy",
          language: "es",
    });
    $('.date-notificar').on('keydown',function(e){
          e.preventDefault();
    });
  </script>


</body>
</html>
