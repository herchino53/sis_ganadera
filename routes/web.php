<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('/auth/login');
    //return view('/home');
    //return 'Hello World';
});



Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::get('home', 'HomeController@index');


Route::get('/form', function (){
    return view("form");
});


Route::view('/login', "login")->name('login');
Route::view('/registro', "register")->name('registro');
Route::view('/privada', "secret")->name('privada');


Route::post('/validar-regitro', [LoginController::class, 'register'])->name('validar-regitro');
Route::post('/inicia-sesion', [LoginController::class, 'login'])->name('inicia-sesion');
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');

//test
/*
Route::get('/chartPdf', 'ChartController@index');
// Route::get('cargar-datos',array('as'=>'excel.import','uses'=>'FileController@importExportExcelORCSV'));
Route::get('cargar-datos', 'FileController@importExportExcelORCSV');
Route::post('import-csv-excel',array('as'=>'import-csv-excel','uses'=>'FileController@importFileIntoDB'));
Route::get('download-excel-file/{type}', array('as'=>'excel-file','uses'=>'FileController@downloadExcelFile'));
*/






//Route::get('/test','testController@index');
//Route::auth();
//Route::get('/dashboard/trabajando', 'DashboardController@trabajando'); // mensaje de trabajando






// ADMIN 
/*
Route::get('/crear-primer-admin', 'DashboardController@createFirstAdminForm');
Route::post('/almacenarAdmin', 'DashboardController@createAdmin');
Route::post('/dashboard/addUser', 'DashboardController@addUser');
Route::get('/dashboard', 'DashboardController@index');
Route::get('/dashboard/mostrar-usuarios', 'DashboardController@showUsers');
Route::get('/dashboard/mostrar-rol-usuario', 'DashboardController@showRol');
Route::get('/dashboard/mostrar-rol', 'DashboardController@showUsers');
Route::get('/dashboard/mostrar-rol-test', 'DashboardController@showRolTest');
*/


//Crud de usuarios
/*
Route::get('/dashboard/crear-usuario', 'DashboardController@showCreateUserForm');
Route::get('/dashboard/editarUsuario/{id}', 'DashboardController@editUserForm');
Route::post('/dashboard/editar-usuario', 'DashboardController@editUser');
Route::get('/dashboard/editarUsuarioActual/{id}', 'DashboardController@editLoginUserForm');
Route::post('/dashboard/editar-usuario-actual', 'DashboardController@editLoginUser');
Route::get('/dashboard/eliminar-usuario/{id}', 'DashboardController@deleteUserMessage');
Route::post('/dashboard/confirmar-eliminacion', 'DashboardController@deleteConfirm');
Route::post('/dashboard/remover-usuario/{id}', 'DashboardController@removeUser');
*/




/*
Route::get('/redirect', function (){
    return back();
});


Route::get('/redirectHome', function (){
    return redirect()->to('/interna');
});
*/

Auth::routes();
?>